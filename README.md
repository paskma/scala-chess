# Scala-Chess #

Naive chess engine implemented in Scala, features Minimax with alpha-beta pruning.

Parallelization included.

## Build Instructions ##

Requires Gradle. Type

`gradle jar`

This generates one jar which depends on scala-library.jar

## How to Run (GUI) ##

```
export CLASSPATH=scala-chess.jar:scala-library.jar
java info.paskma.chess.gui.GuiMain
```

## How to Run (command-line) ##

Usage: `scala-chess -a <algorithm> -d <depth> -m <num_moves_to_print> -c <castlingfile> [-o <out_boardfile>] <boardfile>`

`export CLASSPATH=scala-chess.jar:scala-library.jar`

`java info.paskma.chess.Main -a ParallelAlphaBeta -d 4 -m 5 -c cleancastling.txt -o firstmove.txt cleanboard.txt`

## Helper for Moving Pieces ##

`java info.paskma.chess.ApplyMove cleanboard.txt b1 c3 > knight_moved.txt`

## Tests ##

`java info.paskma.chess.RunTests`
(Not using any real test framework)

## Example of Run ##


```
#!tex

$java info.paskma.chess.Main -a ParallelAlphaBeta -d 4 -m 5 -c cleancastling.txt -o firstmove.txt cleanboard.txt

Input board is:
8♜♞♝♛♚♝♞♜
7♟♟♟♟♟♟♟♟
6--------
5--------
4--------
3--------
2♙♙♙♙♙♙♙♙
1♖♘♗♕♔♗♘♖

Thinking on depth 4 using ParallelAlphaBeta...

Messages: duration -> 3745 positionsEvaluated -> 524580

Possible moves: 20
Best 5 moves:
Pawn(White) e2->e3 (Score 10033)
Pawn(White) e2->e4 (Score 9996)
Pawn(White) c2->c3 (Score 9989)
Pawn(White) c2->c4 (Score 9976)
Pawn(White) d2->d4 (Score 9849)
Best board:
8♜♞♝♛♚♝♞♜
7♟♟♟♟♟♟♟♟
6--------
5--------
4--------
3----♙---
2♙♙♙♙-♙♙♙
1♖♘♗♕♔♗♘♖

```
