package info.paskma.chess.positions

import info.paskma.chess.io.BoardIo
import info.paskma.chess.search.{Minimax, AlphaBeta}
import scala.collection.mutable.ListMap
import info.paskma.chess.board.Color._

/**
 * User: paskma
 * Date: 6/11/14
 */
object OpeningTest {
  def first = {
    val clean = BoardIo.initial

    val start = System.currentTimeMillis()
    val messages = new ListMap[String, Any]

    //val solver = new Minimax
    val solver = new AlphaBeta

    val evaluatedMoves = solver.evaluatedMoves(clean, White, 4, messages).sorted
    val duration = System.currentTimeMillis() - start
    val bestMove = evaluatedMoves(0)

    println(clean.withMovedPiece(bestMove.move))
    println("Messages %s, duration %d.".format(messages, duration))
    val maxMoves = 5
    println("Best %d moves:".format(maxMoves))
    for (i <- 0 until math.min(maxMoves, evaluatedMoves.length)) {
      println(evaluatedMoves(i).description)
    }
  }

  def main(args : Array[String]) : Unit  = {
    first
    println("Done.")
  }
}
