package info.paskma.chess.board

import info.paskma.chess.piece.Pawn
import Color._

object BoardTest {
  def main(args: Array[String]) {
    
    val board = new Board
    val blackPawn = new Pawn(Black)
    val whitePawn = new Pawn(White)
    val b2 = board
      .withNewPiece(blackPawn, 0, 0)
      .withNewPiece(whitePawn, 1, 1)
      .withNewPiece(blackPawn, 2, 2)
    
    assert(-1 == b2.weightScore)
    print(this.getClass().getSimpleName() + " Done.") 
  }
}