package info.paskma.chess

trait AssertionCounter {
  var assertionsCalled = 0;
  
  def assert(b : Boolean) = {
    assertionsCalled += 1
    Predef.assert(b)
  }

}