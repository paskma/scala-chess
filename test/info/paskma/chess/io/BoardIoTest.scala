package info.paskma.chess.io

/**
 * User: paskma
 * Date: 6/4/14
 */
object BoardIoTest {
  def main(args: Array[String]) {
    val boardString =
       """8♜♞♝♛♚♝♞♜
         |7♟♟♟♟♟♟♟♟
         |6--------
         |5--------
         |4--------
         |3--------
         |2♙♙♙♙♙♙♙♙
         |1♖♘♗♕♔♗♘♖""".stripMargin

    println("*" + boardString + "*")
    val board = BoardIo.fromString(boardString)
    val boardString2 = BoardIo.toString(board)
    println(boardString2)
    assert(boardString.trim() == boardString2.trim())
    println(this.getClass().getSimpleName() + " Done.")
  }
}
