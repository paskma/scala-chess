package info.paskma.chess.io

import info.paskma.chess.board.CastlingState
import info.paskma.chess.AssertionCounter

/**
 * User: paskma
 * Date: 6/17/14
 */
object CastlingStateIoTest extends AssertionCounter {
  def serialization = {
    val state = new CastlingState(true, false, true, false)
    val s = CastlingStateIo.toString(state)
    val state2 = CastlingStateIo.fromString(s)

    assert(state.whiteKingSideAvailable == state2.whiteKingSideAvailable)
    assert(state.whiteQueenSideAvailable == state2.whiteQueenSideAvailable)
    assert(state.blackKingSideAvailable == state2.blackKingSideAvailable)
    assert(state.blackQueenSideAvailable == state2.blackQueenSideAvailable)
  }

  def main(args : Array[String]) : Unit = {
    serialization
    print("%s Done - %d assertions called.".format(this.getClass().getSimpleName(), assertionsCalled))
  }
}
