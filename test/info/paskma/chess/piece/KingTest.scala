package info.paskma.chess.piece

import info.paskma.chess.board._
import Color._
import Coords._
import info.paskma.chess.AssertionCounter
import info.paskma.chess.search.{Algorithm, Interceptor, Minimax, AlphaBeta}
import scala.collection.mutable.ListMap
import info.paskma.chess.board.Color
import info.paskma.chess.io.BoardIo
import scala.Predef._

object KingTest extends AssertionCounter {
  def simpleMove = {
    val clean = new Board(CastlingDisabled, false)
    val whiteKing= new King(White)
    val board = clean
      .withNewPiece(whiteKing, 4, 3)
    
    val moves = whiteKing.availableMoves(board, 4, 3)
    assert(moves.length == 8)
    moves.foreach(m => assert(m.toRow != 4 || m.toCol != 3))
  }

  def checkmate = {
    val clean = new Board(CastlingDisabled, false)
    val whiteKing= new King(White)
    val whiteRook= new Rook(White)
    val blackKing= new King(Black)
    
    // 4 
    // 3---♔
    // 2-----♖
    // 1♚-----
    //  ABCDEF
    val board = clean
      .withNewPiece(whiteKing, ROW_3, COL_D)
      .withNewPiece(whiteRook, ROW_2, COL_F)
      .withNewPiece(blackKing, ROW_1, COL_A)

    val start = System.currentTimeMillis()
    val messages = new ListMap[String, Any]
    //val evaluatedMoves = new Minimax().evaluatedMoves(board, White, 6, messages)
    val evaluatedMoves = new AlphaBeta().evaluatedMoves(board, White, 7, messages)
    val duration = System.currentTimeMillis() - start;
    val bestMove = evaluatedMoves.sorted.apply(0)

    println(bestMove.description)
    println("Eval %d, %s, duration %d".format(bestMove.evaluation, messages, duration))
    assert(bestMove.evaluation >= whiteKing.weight + whiteRook.weight)
  }

  def castling = {
    val boardString =
      """8♚-------
        |7--------
        |6--------
        |5--------
        |4--------
        |3--------
        |2-----♙♙♙
        |1♝♝--♔--♖""".stripMargin

    val board = BoardIo.fromString(boardString).withCastling(new CastlingState(true, false, false, false))
    //println(io.toString(board))

    val messages = Algorithm.createMessages
    val evaluatedMoves = new AlphaBeta().evaluatedMoves(board, White, 4, messages)
    val bestMove = evaluatedMoves.sorted.apply(0)
    println(bestMove)
    println(BoardIo.toString(board.withMovedPiece(bestMove.move)))
    assert(bestMove.move.isInstanceOf[CastlingMove])
  }

  def forbiddenCastling = {
    val boardString =
      """8♚-------
        |7--------
        |6--------
        |5--------
        |4--------
        |3--♝-----
        |2-----♙♙♙
        |1♝♝--♔--♖""".stripMargin

    val board = BoardIo.fromString(boardString).withCastling(new CastlingState(true, false, false, false))

    val moves = board.availableMoves(White)
    moves.foreach(m => assert(!m.isInstanceOf[CastlingMove]))
  }
  
  def main(args: Array[String]) {
    simpleMove
    checkmate
    castling
    forbiddenCastling
    print("%s Done - %d assertions called.".format(this.getClass().getSimpleName(), assertionsCalled))
  }
}