package info.paskma.chess.piece

import info.paskma.chess.board.{Coords, Color, Board}
import Color._
import Coords._
import info.paskma.chess.AssertionCounter
import info.paskma.chess.search.Interceptor
import info.paskma.chess.search.Minimax

object RookTest extends AssertionCounter {
  
  def simpleMove = {
    val clean = new Board
    val whiteRook= new Rook(White)
    val board = clean
      .withNewPiece(whiteRook, 4, 3)
    
    val moves = whiteRook.availableMoves(board, 4, 3)
    assert(moves.length == 14)
    moves.foreach(m => assert(m.toRow != 4 || m.toCol != 3))
  }
  
  
  def main(args: Array[String]) {
    simpleMove
    print("%s Done - %d assertions called.".format(this.getClass().getSimpleName(), assertionsCalled))
  }
}