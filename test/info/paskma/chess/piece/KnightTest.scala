package info.paskma.chess.piece

/**
 * User: paskma
 * Date: 6/1/14
 */

import info.paskma.chess.board.{Color, Board}
import Color._
import info.paskma.chess.AssertionCounter
import info.paskma.chess.board.Board


object KnightTest extends AssertionCounter {

  def simpleMove = {
    val clean = new Board
    val whiteKnight = new Knight(White)
    val board = clean
      .withNewPiece(whiteKnight, 0, 0)

    val moves = whiteKnight.availableMoves(board, 0, 0)
    assert(moves.length == 2)
  }

  def main(args: Array[String]) {
    simpleMove
    print("%s Done - %d assertions called.".format(this.getClass().getSimpleName(), assertionsCalled))
  }
}