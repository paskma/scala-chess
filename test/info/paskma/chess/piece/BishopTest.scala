package info.paskma.chess.piece

import info.paskma.chess.board.{Coords, Color, Board}
import Color._
import Coords._
import info.paskma.chess.AssertionCounter
import info.paskma.chess.search.{AlphaBeta, Interceptor, Minimax}
import scala.collection.mutable.ListMap

object BishopTest extends AssertionCounter {

  def simpleMove = {
    val clean = new Board
    val whiteBishop = new Bishop(White)
    val board = clean
      .withNewPiece(whiteBishop, 0, 0)

    val moves = whiteBishop.availableMoves(board, 0, 0)
    assert(moves.length == 7)
    for (x <- 1 to 7) {
      assert(moves(x-1).toCol == x && moves(x-1).toRow == x)
    }
  }

  def simpleMove2 = {
    val clean = new Board
    val whiteBishop = new Bishop(White)
    val board = clean
      .withNewPiece(whiteBishop, 7, 7)

    val moves = whiteBishop.availableMoves(board, 7, 7)
    assert(moves.length == 7)
    var i = 0;
    for (x <- 6 to 0 by -1) {
      assert(moves(i).toCol == x && moves(i).toRow == x)
      i += 1
    }
  }

  def takeRook = {
    val clean = new Board
    val whiteBishop= new Bishop(White)
    val blackRook1 = new Rook(Black)
    val blackRook2 = new Rook(Black)

    // 4
    // 3
    // 2_♗
    // 1__♜♜__
    //  ABCDEF
    val board = clean
      .withNewPiece(whiteBishop, ROW_2, COL_B)
      .withNewPiece(blackRook1, ROW_1, COL_C)
      .withNewPiece(blackRook2, ROW_1, COL_D)

    val start = System.currentTimeMillis()
    val messages = new ListMap[String, Any]
    val evaluatedMoves = new AlphaBeta().evaluatedMoves(board, White, 2, messages)
    val duration = System.currentTimeMillis() - start;
    val bestMove = evaluatedMoves.sorted.apply(0)

    println(bestMove)
    println("Eval %d, %s, duration %d".format(bestMove.evaluation, messages, duration))
    assert(bestMove.move.piece == whiteBishop)
    assert(bestMove.move.toRow == ROW_1)
    assert(bestMove.move.toCol == COL_C)
  }

  def coverage = {
    val clean = new Board
    val whiteBishop= new Bishop(White)

    // 4
    // 3
    // 2_♗
    // 1
    //  ABCDEF
    val board = clean
      .withNewPiece(whiteBishop, ROW_2, COL_B)
      .withNewPiece(new Pawn(White), ROW_5, COL_E)
      .withNewPiece(new Pawn(Black), ROW_3, COL_A)

    //println(whiteBishop.coverage(board, ROW_2, COL_B))
    assert(whiteBishop.coverage(board, ROW_2, COL_B) == 9 - 4)
  }

  def main(args: Array[String]) {
    simpleMove
    simpleMove2
    takeRook
    coverage
    print("%s Done - %d assertions called.".format(this.getClass().getSimpleName(), assertionsCalled))
  }
}