package info.paskma.chess.piece

import info.paskma.chess.board.{Coords, Color, Board}
import Color._
import Coords._
import info.paskma.chess.AssertionCounter
import info.paskma.chess.search.Interceptor
import info.paskma.chess.search.Minimax
import scala.collection.mutable.ListMap

object PawnTest extends AssertionCounter {
  def twoMoves = {
    val clean = new Board
    val whitePawn = new Pawn(White)
    val board = clean
      .withNewPiece(whitePawn, 1, 0)
    
    val m = whitePawn.availableMoves(board, 1, 0)
    val boards = m.map(move => board.withMovedPiece(move))
    
    assert(boards.length == 2)
    assert(boards(0).weightScore == whitePawn.weight)
    assert(boards(1).weightScore == whitePawn.weight)
  }
  
  def simpleTake = {
    val clean = new Board
    val whitePawn1 = new Pawn(White)
    val whitePawn2 = new Pawn(White)
    val victimPawn = new Pawn(Black)
    
    val board = clean
      .withNewPiece(whitePawn1, 2, 0)
      .withNewPiece(whitePawn2, 3, 0)
      .withNewPiece(victimPawn, 3, 1)
    
    val m = whitePawn1.availableMoves(board, 2, 0)
    val boards = m.map(move => board.withMovedPiece(move))
    
    //print(board.getScore)
    assert(board.weightScore == 1)
    //print(boards.length)
    assert(boards.length == 1)
    assert(boards(0).weightScore == 2)
    assert(boards(0).isWhitePiece(3, 1))
  }
  
  def simpleDecision = {
    val clean = new Board
    val whitePawn = new Pawn(White)
    val victimPawn = new Pawn(Black)
    
    //
    // 3 ♟
    // 2♙
    // 1
    //  AB
    val board = clean
      .withNewPiece(whitePawn, ROW_2, COL_A)
      .withNewPiece(victimPawn, ROW_3, COL_B)
  
    val messages = new ListMap[String, Any]
    val evaluatedMoves = new Minimax().evaluatedMoves(board, White, 0, messages).sorted
    val bestMove = evaluatedMoves(0)
    
    //println(messages)
    assert(evaluatedMoves.length == 3)
    assert(board.score == 0)
    assert(board.withMovedPiece(bestMove.move).weightScore == 1)
    evaluatedMoves.drop(1).foreach(move => assert(move.evaluation  == 0))
  }
  
  def avoidance = {
    val clean = new Board
    val whitePawn = new Pawn(White)
    val blackPawn = new Pawn(Black)
    
    // 4 ♟
    // 3 
    // 2♙
    // 1
    //  AB
    val board = clean
      .withNewPiece(whitePawn, ROW_2, COL_A)
      .withNewPiece(blackPawn, ROW_4, COL_B)
    
    val messages = new ListMap[String, Any]
    val evaluatedMoves = new Minimax().evaluatedMoves(board, White, 1, messages).sorted
    val bestMove = evaluatedMoves(0)
    
    println(messages)
    assert(evaluatedMoves.length == 2)
    //println(evaluatedMoves.mkString(", "))
    assert(bestMove.move.toRow == ROW_4)
    assert(bestMove.move.toCol == COL_A)
  }
  
  
  def main(args: Array[String]) {
    twoMoves
    simpleTake
    simpleDecision
    avoidance
    print("%s Done - %d assertions called.".format(this.getClass().getSimpleName(), assertionsCalled))
  }
}