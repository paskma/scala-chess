package info.paskma.chess.search

import info.paskma.chess.io.BoardIo
import info.paskma.chess.board.Color._
import info.paskma.chess.AssertionCounter

/**
 * User: paskma
 * Date: 6/21/14
 */
object ParallelAlphaBetaTest extends AssertionCounter {
  def sane = {
    val board = BoardIo.initial

    val seqStart = System.currentTimeMillis()
    val seqAlg = new AlphaBeta
    val seqMsg = Algorithm.createMessages
    val seqMoves = seqAlg.evaluatedMoves(board, White, 4, seqMsg).sorted

    val parStart = System.currentTimeMillis()
    val parAlg = new ParallelAlphaBeta
    val parMsg = Algorithm.createMessages
    val parMoves = parAlg.evaluatedMoves(board, White, 4, parMsg).sorted
    val end = System.currentTimeMillis()

    assert(seqMoves(0).toString() == parMoves(0).toString())
    assert(seqMsg("positionsEvaluated") == parMsg("positionsEvaluated"))
    val seqDuration = parStart - seqStart
    val parDuration = end - parStart
    println("Seq, par, time: %d, %d,  ms".format(seqDuration, parDuration))
    println("Speedup %1.2f.".format(seqDuration.toDouble / parDuration.toDouble))
  }

  def main(args : Array[String]) = {
    sane
    print("%s Done - %d assertions called.".format(this.getClass().getSimpleName(), assertionsCalled))
  }

}
