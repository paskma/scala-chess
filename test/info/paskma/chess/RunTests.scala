package info.paskma.chess

import info.paskma.chess.piece._
import info.paskma.chess.board.BoardTest
import info.paskma.chess.io.BoardIoTest

/**
 * Guerilla test runner.
 * User: paskma
 * Date: 6/8/14
 */
object RunTests {
  def main(args : Array[String]) : Unit = {
    BoardTest.main(args)
    BoardIoTest.main(args)
    BishopTest.main(args)
    KingTest.main(args)
    KnightTest.main(args)
    PawnTest.main(args)
    RookTest.main(args)
  }
}
