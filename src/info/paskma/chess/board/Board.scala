package info.paskma.chess.board

import Color._
import info.paskma.chess.piece.{King, Piece}
import scala.collection.immutable.VectorBuilder
import info.paskma.chess.io.BoardIo


class Board(
             val castlingState : CastlingState,
             val kingTaken : Boolean) extends Validation {

  private val fields = Array.ofDim[Piece](8, 8)

  def this() = this(new CastlingState, false)

  override def hashCode() : Int = {
    var result = castlingState.hashCode()
    var counter = 0

    fields.foreach(row => {
      row.foreach(piece => {
        counter += 1
        if (piece != null)
          result += result * 37 + piece.toSymbol.asInstanceOf[Int] + counter
      })
    })

    result
  }

  override def equals(x : Any) : Boolean = {
    val b = x.asInstanceOf[Board]

    if (!castlingState.equals(b.castlingState))
      return false

    if (kingTaken != b.kingTaken)
      return false

    for (i <- fields.indices) {
      for (j <- fields(i).indices) {
        if (getPiece(i, j) != b.getPiece(i, j))
          return false
      }
    }

    return true
  }

  /**
   * Score from the point of White player
   */
  def weightScore : Long = {
    var result : Long = 0

    fields.foreach(row => {
      row.foreach(piece => {
          if (piece != null)
            result += piece.weightScore
      })
    })
    
    result
  }

  protected def weightPlusCoverageScore : Long = {
    var result : Long = 0

    for (a <- fields.indices) {
      for (b <- fields(a).indices) {
        val piece = getPiece(a, b)
        if (piece != null) {
          val c = piece.coverage(this, a, b)
          val w = piece.weightScore // signed
          result += w * (10000 + c)
        }
      }
    }

    result
  }

  def score : Long = weightPlusCoverageScore

  def booleanCoverage(color : Color) : Array[Array[Boolean]] = {
    val result = Array.ofDim[Boolean](8, 8)
    val moves = availableStraitMoves(color)
    moves.foreach(m => {
      result(m.toRow)(m.toCol) = true
    })
    result
  }

  protected def coverageSum(color : Color) : Int = {
    val possible = booleanCoverage(color)

    var result = 0
    possible.foreach(row => {
      row.foreach(covered => {
        if (covered)
          result += 1
      })
    })

    result
  }
  
  def availableMoves(color : Color) : Vector[Move] = {
    val result = new VectorBuilder[Move]
    
    for (a <- fields.indices) {
      for (b <- fields(a).indices) {
        val piece = getPiece(a, b)
        if (piece != null)
        {
          if (piece.color == color)
            result ++= piece.availableMoves(this, a, b)
        }
      }
    }
    
    result.result()
  }

  def availableStraitMoves(color : Color) : Vector[Move] = {
    val result = new VectorBuilder[Move]

    for (a <- fields.indices) {
      for (b <- fields(a).indices) {
        val piece = getPiece(a, b)
        if (piece != null)
        {
          if (piece.color == color)
            result ++= piece.availableStraitMoves(this, a, b)
        }
      }
    }

    result.result()
  }
  
  def withNewPiece(p : Piece, row : Int, col : Int) : Board = {
    val result = new Board(castlingState, kingTaken)
    result.copyPieces(this.fields)    
    result.fields(row)(col) = p
    result
  }

  def withCastling(newCastlingState : CastlingState) : Board = {
    val result = new Board(newCastlingState, kingTaken)
    result.copyPieces(this.fields)
    result
  }
  
  def withMovedPiece(move : Move) : Board = {
    val p = getPiece(move.toRow, move.toCol)
    val takesKing = (p != null && p.isInstanceOf[King] && !move.piece.isInstanceOf[King])

    val result = new Board(castlingState.afterMoveOf(move.piece, move.fromCol), takesKing)
    result.copyPieces(this.fields)
    move.execute(result.fields)
    result
  }

  private def copyPieces(source : Array[Array[Piece]]) = {
    for (a <- fields.indices) {
      System.arraycopy(source(a), 0, fields(a), 0, fields.length)
    }
  }
  
  def getPiece(row : Int, col : Int) : Piece = fields(row)(col)
  
  def isFree(row : Int, col : Int) : Boolean = getPiece(row, col) == null
  
  def isFree(rows : Range, col: Int) : Boolean = {
    for (row <- rows) {
      if (!isFree(row, col))
        return false
    }
    true
  }

  def isFree(row : Int, cols : Range) : Boolean = {
    for (col <- cols) {
      if (!isFree(row, col))
        return false
    }
    true
  }
  
  def isBlackPiece(row : Int, col : Int) : Boolean = isPieceOfColor(row, col, Black)
  def isWhitePiece(row : Int, col : Int) : Boolean = isPieceOfColor(row, col, White)
  
  def isPieceOfColor(row : Int, col : Int, color : Color) : Boolean = {
    val p = getPiece(row, col)
    p != null && p.color == color
  }

  override def toString: String = BoardIo.toString(this)
}