package info.paskma.chess.board

import info.paskma.chess.piece.Piece

class Move(val piece : Piece,
    val fromRow : Int, 
    val fromCol : Int, 
    val toRow : Int, 
    val toCol : Int) extends Validation {

  assert(isValid(fromRow, fromCol))
  assert(isValid(toRow, toCol))

  def execute(fields : Array[Array[Piece]]) : Unit = {
    assert(fields(fromRow)(fromCol) == piece)

    fields(toRow)(toCol) = piece
    fields(fromRow)(fromCol) = null
  }

  def this(move : Move) = this(move.piece, move.fromRow, move.fromCol, move.toRow, move.toCol)

  override def toString() = "%s[%d,%d]->[%d,%d]".format(piece, fromRow, fromCol, toRow, toCol)

  def description : String = {
    "%s %c%d->%c%s".format(
      piece,
      Coords.colIndexToLetter(fromCol),
      fromRow + 1,
      Coords.colIndexToLetter(toCol),
      toRow + 1
    )
  }
  
}