package info.paskma.chess.board

class EvaluatedMove(
    val move : Move,
    val evaluation : Long) extends Ordered[EvaluatedMove] {
  
  /**
   * Move with the highest evaluation goes first.
   */
  def compare(that: EvaluatedMove) : Int = that.evaluation.compareTo(this.evaluation)
  
  override def toString() =  "%s%d".format(move.toString, evaluation)

  def description : String = "%s (Score %d)".format(move.description, evaluation)

}