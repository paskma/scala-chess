package info.paskma.chess.board

import info.paskma.chess.piece.{Rook, King, Piece}

/**
 * User: paskma
 * Date: 6/8/14
 */
class CastlingMove(
  king : King,
  fromRow : Int,
  fromCol : Int,
  toRow : Int,
  toCol : Int) extends Move(king, fromRow, fromCol, toRow, toCol) {

  override def toString() : String = "Castling " + super.toString()

  override def execute(fields : Array[Array[Piece]]) : Unit = {
    super.execute(fields)
    assert(toRow == fromRow)

    if (toCol > fromCol) {
      // king side
      val rook = fields(toRow)(7).asInstanceOf[Rook]
      fields(toRow)(toCol - 1) = rook
      fields(toRow)(7) = null
    } else {
      // queen side
      val rook = fields(toRow)(0).asInstanceOf[Rook]
      fields(toRow)(toCol + 1) = rook
      fields(toRow)(0) = null
    }
  }
}
