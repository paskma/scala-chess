package info.paskma.chess.board

trait Validation {
  def isValid(i : Int, j : Int) : Boolean = {
    val r = 0 to 7
    return r.contains(i) && r.contains(j)
  }
}