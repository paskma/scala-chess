package info.paskma.chess.board

import info.paskma.chess.piece.{Rook, King, Piece}

/**
 * User: paskma
 * Date: 6/7/14
 */
case class CastlingState(val whiteKingSideAvailable : Boolean,
                    val whiteQueenSideAvailable : Boolean,
                    val blackKingSideAvailable : Boolean,
                    val blackQueenSideAvailable : Boolean) {

  def this() = this(true, true, true, true)

  def this(c : CastlingState,
            disableWhiteKingSide : Boolean = false,
            disableWhiteQueenSide : Boolean = false,
            disableBlackKingSide : Boolean = false,
            disableBlackQueenSide : Boolean = false) = {
    this(
      c.whiteKingSideAvailable && !disableWhiteKingSide,
      c.whiteQueenSideAvailable && !disableWhiteQueenSide,
      c.blackKingSideAvailable && !disableBlackKingSide,
      c.blackQueenSideAvailable && !disableBlackQueenSide)
  }

  def afterMoveOf(p : Piece, sourceCol : Int) : CastlingState = {
    if (p.isInstanceOf[King]) {
      if (p.isWhite)
        return new CastlingState(this, disableWhiteKingSide = true, disableWhiteQueenSide = true)
      else
        return new CastlingState(this, disableBlackKingSide = true, disableBlackQueenSide = true)
    } else if (p.isInstanceOf[Rook]) {
      if (p.isWhite) {
        if (sourceCol == 0)
          return new CastlingState(this, disableWhiteQueenSide = true)
        else if (sourceCol == 7)
          return new CastlingState(this, disableWhiteKingSide = true)
        else
          return this
      } else {
        if (sourceCol == 0)
          return new CastlingState(this, disableBlackQueenSide = true)
        else if (sourceCol == 7)
          return new CastlingState(this, disableBlackKingSide = true)
        else
          return this
      }
    } else {
      return this
    }
  }
}

object CastlingDisabled extends CastlingState(false, false, false, false) {

}