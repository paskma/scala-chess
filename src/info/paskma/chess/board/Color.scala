package info.paskma.chess.board

object Color extends Enumeration {
  type Color = Value
  val White, Black = Value
  
  def other(color : Color) : Color = {
    color match {
      case White => Black
      case Black => White
    }
  }
}