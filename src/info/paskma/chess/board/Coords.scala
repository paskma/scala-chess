package info.paskma.chess.board

/** Logical coordinates */
object Coords {
  val ROW_1 = 0
  val ROW_2 = 1
  val ROW_3 = 2
  val ROW_4 = 3
  val ROW_5 = 4
  val ROW_6 = 5
  val ROW_7 = 6
  val ROW_8 = 7
  
  val COL_A = 0
  val COL_B = 1
  val COL_C = 2
  val COL_D = 3
  val COL_E = 4
  val COL_F = 5
  val COL_G = 6
  val COL_H = 7

  def colIndexToLetter(col : Int) : Char = ('a' + col).asInstanceOf[Char]
  def letterToColIndex(letter : Char) : Int = letter.asInstanceOf[Int] - 'a'.asInstanceOf[Int]
  def parseCoord(coord : String) : (Int, Int) = (coord.substring(1).toInt - 1, letterToColIndex(coord(0)))
}