package info.paskma.chess.io

import info.paskma.chess.piece._
import info.paskma.chess.board.{Color, Board}
import Color._
import info.paskma.chess.board.Board
import java.io.PrintWriter

/**
 * User: paskma
 * Date: 6/1/14
 */
object BoardIo {
  def pieceFromChar(c : Char) : Piece = {
    c match {
      case King.symbolWhite => new King(White)
      case Queen.symbolWhite => new Queen(White)
      case Rook.symbolWhite => new Rook(White)
      case Knight.symbolWhite => new Knight(White)
      case Bishop.symbolWhite => new Bishop(White)
      case Pawn.symbolWhite => new Pawn(White)

      case King.symbolBlack => new King(Black)
      case Queen.symbolBlack => new Queen(Black)
      case Rook.symbolBlack => new Rook(Black)
      case Knight.symbolBlack => new Knight(Black)
      case Bishop.symbolBlack => new Bishop(Black)
      case Pawn.symbolBlack => new Pawn(Black)
      case _   => null
    }
  }

  /**
   *
   * 3--♖
   * 2--♖
   * 1---
   *  ABC
   *
   * @param s
   * @return
   */
  def fromString(s : String) : Board = {
    var i = 0
    val ignore = Set(' ', '\r', '\n')
    var board = new Board

    for (row <- 7 to 0 by -1) {
      for (col <- 0 to 8) {
        while(ignore contains s(i)) {
          i += 1
        }

        if (col != 0) {
          val piece = pieceFromChar(s(i))
          if (piece != null)
            board = board.withNewPiece(piece, row, col - 1)
        }
        i += 1
      }
    }

    board
  }

  def toString(board : Board) : String = {
    val result = new StringBuilder()
    for (row <- 7 to 0 by -1) {
      for (col <- 0 to 7) {
        if (col == 0)
          result.append(row + 1)
        val piece = board.getPiece(row, col)
        if (piece == null)
          result.append('-')
        else
          result.append(piece.toSymbol)
        if (col == 7)
          result.append("\n")
      }
    }

    return result.toString
  }

  def fromFile(boardfile : String) : Board = {
    val boardSource = scala.io.Source.fromFile(boardfile)
    val board = BoardIo.fromString(boardSource.mkString)
    boardSource.close()

    board
  }

  def toFile(board : Board, boardfile : String) = {
    val writer = new PrintWriter(boardfile)
    writer.write(board.toString())
    writer.close()
  }


  def initial : Board = {
    val boardString =
      """8♜♞♝♛♚♝♞♜
        |7♟♟♟♟♟♟♟♟
        |6--------
        |5--------
        |4--------
        |3--------
        |2♙♙♙♙♙♙♙♙
        |1♖♘♗♕♔♗♘♖""".stripMargin

    fromString(boardString)
  }
}