package info.paskma.chess.io

import info.paskma.chess.board.CastlingState
import scala.collection.mutable

/**
 * User: paskma
 * Date: 6/17/14
 */
object CastlingStateIo {
  val key_whiteKingSideCastling = "whiteKingSideCastling"
  val key_whiteQueenSideCastling = "whiteQueenSideCastling"
  val key_blackKingSideCastling = "blackKingSideCastling"
  val key_blackQueenSideCastling = "blackQueenSideCastling"

  def toString(state : CastlingState) : String = {
    val items = List(
      (key_whiteKingSideCastling, state.whiteKingSideAvailable),
      (key_whiteQueenSideCastling, state.whiteQueenSideAvailable),
      (key_blackKingSideCastling, state.blackKingSideAvailable),
      (key_blackQueenSideCastling, state.blackQueenSideAvailable))

    val result = new mutable.StringBuilder()
    items.foreach(rec => {
      result.append(rec._1).append("=").append(rec._2).append("\r\n")
    })

    result.toString()
  }

  def fromString(s : String) : CastlingState = {
    var whiteKing = true
    var whiteQueen = true
    var blackKing = true
    var blackQueen = true

    s.lines.foreach(line => {
      if (line.startsWith(key_whiteKingSideCastling)) whiteKing = line.endsWith(true.toString)
      if (line.startsWith(key_whiteQueenSideCastling)) whiteQueen = line.endsWith(true.toString)
      if (line.startsWith(key_blackKingSideCastling)) blackKing = line.endsWith(true.toString)
      if (line.startsWith(key_blackQueenSideCastling)) blackQueen = line.endsWith(true.toString)
    })

    return new CastlingState(whiteKing, whiteQueen, blackKing, blackQueen)
  }
}
