package info.paskma.chess

import info.paskma.chess.board.{Color, Board}
import Color._
import info.paskma.chess.piece.Pawn
import info.paskma.chess.board.Board
import info.paskma.chess.io.{CastlingStateIo, BoardIo}
import info.paskma.chess.search._
import scala.collection.mutable
import scala.annotation.tailrec

object Main {
  type OptionMap = Map[Symbol, Any]
  val usage = """
    Usage: scala-chess -a <algorithm> -d <depth> -m <num_moves_to_print> -c <castlingfile> [-o <out_boardfile>] <boardfile>
              """
  @tailrec
  def nextOption(map : OptionMap, list: List[String]) : OptionMap = {
    list match {
      case Nil => map
      case "-a" :: value :: tail =>
        nextOption(map ++ Map('algorithm -> value), tail)
      case "-c" :: value :: tail =>
        nextOption(map ++ Map('castlingfile -> value), tail)
      case "-d" :: value :: tail =>
        nextOption(map ++ Map('depth -> value.toInt), tail)
      case "-m" :: value :: tail =>
        nextOption(map ++ Map('maxmoves -> value.toInt), tail)
      case "-o" :: value :: tail =>
        nextOption(map ++ Map('outboardfile -> value), tail)
      case string :: Nil =>  nextOption(map ++ Map('boardfile -> string), list.tail)
      case option :: tail => println("Unknown option "+option)
        System.exit(1)
        return map
    }
  }

  def algorithmByName(name : String) : Algorithm = {
    name match {
      case "AlphaBeta" => new AlphaBeta
      case "ParallelAlphaBeta" => new ParallelAlphaBeta
      case "Minimax" => new Minimax
      case "CachingAlphaBeta" => new CachingAlphaBeta
      case _ => throw new IllegalArgumentException("Unknown algorithm " + name)
    }
  }


  def main(args: Array[String]) : Unit = {
    if (args.length == 0) {
      println(usage)
      return
    }
    val arglist = args.toList
    val options = nextOption(Map(),arglist)
    val castlingFile : String = options('castlingfile).asInstanceOf[String]
    val boardfile : String = options('boardfile).asInstanceOf[String]
    val depth : Int = options('depth).asInstanceOf[Int]

    val castlingSource = scala.io.Source.fromFile(castlingFile)
    val castling = CastlingStateIo.fromString(castlingSource.mkString)
    castlingSource.close()

    val board = BoardIo.fromFile(boardfile).withCastling(castling)

    println("Input board is:")
    println(BoardIo.toString(board))

    val solver = algorithmByName(options('algorithm).asInstanceOf[String])
    val messages = new mutable.ListMap[String, Any]
    println("Thinking on depth %d using %s...".format(depth, solver.getClass.getSimpleName))
    val startTime = System.currentTimeMillis()
    val moves = solver.evaluatedMoves(board, White, depth, messages).sorted
    messages += (("duration", (System.currentTimeMillis() - startTime)))
    println("Messages: %s".format(messages.mkString(" ")))
    println("Possible moves: %d".format(moves.length))
    val maxMoves = options('maxmoves).asInstanceOf[Int]
    println("Best %d moves:".format(maxMoves))
    for (i <- 0 until math.min(maxMoves, moves.length)) {
      println(moves(i).description)
    }


    if (moves.nonEmpty) {
      val bestBoard = board.withMovedPiece(moves(0).move)
      println("Best board:")
      println(bestBoard)

      if (options.contains('outboardfile))
        BoardIo.toFile(bestBoard, options('outboardfile).asInstanceOf[String])
    }
  }
}