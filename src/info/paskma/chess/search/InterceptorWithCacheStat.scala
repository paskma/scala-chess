package info.paskma.chess.search

/**
 * Created by paskma on 11/10/14.
 */
class InterceptorWithCacheStat extends Interceptor {
  var cachedScoreUsed : Int = 0

  def cacheHit() = cachedScoreUsed += 1
}
