package info.paskma.chess.search

import info.paskma.chess.board.{EvaluatedMove, Color, Board}
import Color._

import scala.collection.mutable
import info.paskma.chess.search.Algorithm.Messages

trait Algorithm {

  def evaluatedMoves(
      board : Board, 
      player : Color, 
      depth : Int, 
      messages : Messages) : Vector[EvaluatedMove]
}

object Algorithm {
  type Messages = mutable.Map[String, Any]

  def createMessages : Messages = {
    return new mutable.ListMap[String, Any]
  }
}