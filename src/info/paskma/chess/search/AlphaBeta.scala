package info.paskma.chess.search

import info.paskma.chess.board.{EvaluatedMove, Color, Board}
import Color._
import Algorithm.Messages
import scala.util.control.Breaks._

class AlphaBeta extends Algorithm {

  def evaluatedMoves(
      board : Board, 
      player : Color, 
      depth : Int, 
      messages : Messages) : Vector[EvaluatedMove] = {
    
    val interceptor = new Interceptor
    
    val moves = board.availableMoves(player)
    val result = moves.map(move => {
      val evaluation = evaluate(board.withMovedPiece(move), Color.other(player), 
          depth,
          Long.MinValue,
          Long.MaxValue,
          interceptor)
      new EvaluatedMove(move, evaluation)
    })
    
    messages += (("positionsEvaluated", interceptor.positionsEvaluated))
    return result
  }
  
  
  protected def evaluate(board : Board,
      player : Color,
      depth : Int,
      alpha : Long,
      beta : Long,
      interceptor : Interceptor) : Long = {
    
    if (depth == 0 || board.kingTaken) {
      interceptor.evaluated()
      return board.score
    }
    
    val boards = board.availableMoves(player).map(move => board.withMovedPiece(move))
    
    if (boards.length == 0) {
      // terminal point
      interceptor.evaluated()
      return board.score
    }
    
    
    if (player == White) {
      var resultAlpha = alpha
      breakable { for (board <- boards) {
        resultAlpha = math.max(resultAlpha, evaluate(board, Black, depth - 1, resultAlpha, beta, interceptor))
        if (beta <= resultAlpha)
          break
      }}
      return resultAlpha
    } else {
      var resultBeta = beta
      breakable { for (board <- boards) {
        resultBeta = math.min(resultBeta, evaluate(board, White, depth - 1, alpha, resultBeta, interceptor))
        if (resultBeta <= alpha)
          break
      }}
      return resultBeta;
    }      
  } 
}