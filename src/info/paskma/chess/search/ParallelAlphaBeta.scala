package info.paskma.chess.search

import info.paskma.chess.board.{Move, Color, EvaluatedMove, Board}
import info.paskma.chess.board.Color._
import info.paskma.chess.search.Algorithm._

/**
 * User: paskma
 * Date: 6/21/14
 */
class ParallelAlphaBeta extends AlphaBeta {
  override def evaluatedMoves(
                      board : Board,
                      player : Color,
                      depth : Int,
                      messages : Messages) : Vector[EvaluatedMove] = {

    val moves = board.availableMoves(player)
    class MoveWithInterceptor(val move : Move, val interceptor : Interceptor)

    val movesWithInterceptor = moves.map(move => new MoveWithInterceptor(move, new Interceptor))

    val result = movesWithInterceptor.par.map(m => {
      val evaluation = evaluate(board.withMovedPiece(m.move), Color.other(player),
        depth,
        Long.MinValue,
        Long.MaxValue,
        m.interceptor)
      new EvaluatedMove(m.move, evaluation)
    }).toVector

    val positionsEvaluated = movesWithInterceptor.foldLeft(0)((acc, m) => acc + m.interceptor.positionsEvaluated)

    messages += (("positionsEvaluated", positionsEvaluated))
    return result
  }
}
