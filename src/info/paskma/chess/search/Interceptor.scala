package info.paskma.chess.search

class Interceptor {
  var positionsEvaluated : Int = 0
  
  def evaluated() = positionsEvaluated += 1

}