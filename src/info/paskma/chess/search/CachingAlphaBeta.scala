package info.paskma.chess.search

import info.paskma.chess.board.{Color, EvaluatedMove, Board}
import info.paskma.chess.board.Color._
import info.paskma.chess.search.Algorithm._

import scala.collection.parallel.mutable.ParHashMap
import scala.util.control.Breaks._

/**
 * Created by paskma on 11/10/14.
 */
class CachingAlphaBeta extends Algorithm {

  private var cache : ParHashMap[Board, Long] = null

  def evaluatedMoves(
                      board : Board,
                      player : Color,
                      depth : Int,
                      messages : Messages) : Vector[EvaluatedMove] = {

    val interceptor = new InterceptorWithCacheStat
    cache = new ParHashMap[Board, Long]

    val moves = board.availableMoves(player)
    val result = moves.map(move => {
      val evaluation = evaluate(board.withMovedPiece(move), Color.other(player),
        depth,
        Long.MinValue,
        Long.MaxValue,
        interceptor)
      new EvaluatedMove(move, evaluation)
    })

    messages += (("positionsEvaluated", interceptor.positionsEvaluated))
    messages += (("cachedScoreUsed", interceptor.cachedScoreUsed))
    messages += (("cacheSize", cache.size))
    return result
  }


  protected def evaluate(board : Board,
                         player : Color,
                         depth : Int,
                         alpha : Long,
                         beta : Long,
                         interceptor : InterceptorWithCacheStat) : Long = {

    if (depth == 0 || board.kingTaken) {
      val cached = cache.get(board)
      cached match {
        case Some(score) => {
          interceptor.cacheHit()
          return score
        }
        case None => {
          interceptor.evaluated()
          val score = board.score
          cache += ((board, score))
          return score
        }
      }
    }

    val boards = board.availableMoves(player).map(move => board.withMovedPiece(move))

    if (boards.length == 0) {
      // terminal point
      val cached = cache.get(board)
      cached match {
        case Some(score) => {
          interceptor.cacheHit()
          return score
        }
        case None => {
          interceptor.evaluated()
          val score = board.score
          cache += ((board, score))
          return score
        }
      }
    }


    if (player == White) {
      var resultAlpha = alpha
      breakable { for (board <- boards) {
        resultAlpha = math.max(resultAlpha, evaluate(board, Black, depth - 1, resultAlpha, beta, interceptor))
        if (beta <= resultAlpha)
          break
      }}
      return resultAlpha
    } else {
      var resultBeta = beta
      breakable { for (board <- boards) {
        resultBeta = math.min(resultBeta, evaluate(board, White, depth - 1, alpha, resultBeta, interceptor))
        if (resultBeta <= alpha)
          break
      }}
      return resultBeta
    }
  }
}
