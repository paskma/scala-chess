package info.paskma.chess.search

import info.paskma.chess.board.{EvaluatedMove, Color, Board}
import Color._

class Minimax extends Algorithm {

  def evaluatedMoves(
      board : Board, 
      player : Color, 
      depth : Int, 
      messages : Algorithm.Messages) : Vector[EvaluatedMove] = {
    
    val interceptor = new Interceptor
    
    val moves = board.availableMoves(player)
    val result = moves.map(move => {
      val evaluation = evaluate(board.withMovedPiece(move), Color.other(player), depth, interceptor)
      new EvaluatedMove(move, evaluation)
    })
    
    messages += (("positionsEvaluated", interceptor.positionsEvaluated))
    return result
  }
  
  
  private def evaluate(board : Board, player : Color, depth : Int, interceptor : Interceptor) : Long = {
    
    if (depth == 0 || board.kingTaken) {
      interceptor.evaluated()
      return board.score
    }
    
    val boards = board.availableMoves(player).map(move => board.withMovedPiece(move))
    
    if (boards.isEmpty) {
      // terminal point
      interceptor.evaluated()
      return board.score
    }
    
    
    val evaluations = boards.map(board => evaluate(board, Color.other(player), depth - 1, interceptor))
    
    if (player == White)
      return evaluations.max
    else
      return evaluations.min
      
  } 
}