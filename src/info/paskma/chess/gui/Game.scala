package info.paskma.chess.gui

import info.paskma.chess.Main.algorithmByName
import info.paskma.chess.board.{Board, Move}
import info.paskma.chess.board.Color.White
import info.paskma.chess.io.BoardIo

import scala.collection.mutable

/**
  * Created by paskma on 4/15/17.
  */
class Game {
  private val boards = new mutable.Stack[Board]
  private val computerMoves = new mutable.Stack[Move]
  boards.push(BoardIo.initial)

  def board: Board = boards.top
  def pushBoard(b: Board) : Unit = boards.push(b)
  def lastComputerMove = computerMoves.top

  def computerPlayWhite(): Unit = {
    val solver = algorithmByName("ParallelAlphaBeta")
    val depth = 4
    val messages = new mutable.ListMap[String, Any]
    println("Thinking on depth %d using %s...".format(depth, solver.getClass.getSimpleName))
    val startTime = System.currentTimeMillis()
    val moves = solver.evaluatedMoves(board, White, depth, messages).sorted
    messages += (("duration", (System.currentTimeMillis() - startTime)))
    println("Messages: %s".format(messages.mkString(" ")))
    println("Possible moves: %d".format(moves.length))
    val maxMoves = 6
    println("Best %d moves:".format(maxMoves))
    for (i <- 0 until math.min(maxMoves, moves.length)) {
      println(moves(i).description)
    }


    if (moves.nonEmpty) {
      val bestBoard = board.withMovedPiece(moves(0).move)
      println("Best board:")
      println(bestBoard)
      pushBoard(bestBoard)
      computerMoves.push(moves(0).move)
    }

  }
}
