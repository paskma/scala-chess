package info.paskma.chess.gui

import java.awt.event.{ActionEvent, ActionListener}
import java.awt.{BorderLayout, Dimension}
import javax.swing._

import info.paskma.chess.board.Color

/**
  * Created by paskma on 4/14/17.
  */
class MainWindow extends JFrame("Scala-Chess") {
  setPreferredSize(new Dimension(800, 600))
  val boardPanel = new BoardPanel(Color.Black)
  val game = new Game
  boardPanel.setBoard(game.board)
  getContentPane().add(boardPanel, BorderLayout.CENTER)

  val buttons = new JPanel()
  buttons.setLayout(new BoxLayout(buttons, BoxLayout.Y_AXIS))
  buttons.add(Box.createRigidArea(new Dimension(100, 0)))
  val whiteMove = new JButton("Computer, play white")
  whiteMove.addActionListener(new ActionListener() {
    override def actionPerformed(actionEvent: ActionEvent): Unit = {
      computerPlayWhite()
    }
  })
  buttons.add(whiteMove)
  val status = new JLabel
  buttons.add(status)
  getContentPane.add(buttons, BorderLayout.EAST)
  pack()

  def computerPlayWhite(): Unit = {
    game.pushBoard(boardPanel.board)
    status.setText("Thinking...")
    whiteMove.setEnabled(false)
    new SwingWorker[Unit, Object] {
      override def doInBackground(): Unit = {
        game.computerPlayWhite()
      }

      override def done(): Unit = {
        boardPanel.setBoard(game.board)
        status.setText(game.lastComputerMove.description)
        whiteMove.setEnabled(true)
      }
    }.execute()
  }
}

class OpenMainWindow extends Runnable {
  override def run(): Unit = {
    val window = new MainWindow
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    window.setVisible(true)
  }
}


object GuiMain {
  def main(args: Array[String]) : Unit = {
    SwingUtilities.invokeLater(new OpenMainWindow)
    System.out.println("Done.")
  }
}