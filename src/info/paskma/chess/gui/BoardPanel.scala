package info.paskma.chess.gui

import java.awt.event.{MouseAdapter, MouseEvent, MouseListener}
import java.awt.{Color, Font, GridLayout}
import javax.swing.{JLabel, JPanel, SwingConstants}

import info.paskma.chess.board.{Board, Coords, Move}

import scala.collection.mutable

/**
  * Created by paskma on 4/14/17.
  */
class BoardPanel(val playerColor: info.paskma.chess.board.Color.Color) extends JPanel {
  var board: Board = null
  val darkColor = Color.GRAY
  val lightColor = Color.LIGHT_GRAY
  val selectedColor = new Color(100, 100, 200)
  setLayout(new GridLayout(9, 9))
  val indexToLabel = new mutable.HashMap[(Int, Int), (JLabel, Color)]
  var selectedField: Option[(Int, Int)] = Option.empty
  insertLabels()

  for (col <- 1 to 8) {
    indexToLabel.get(8, col).foreach(
      _._1.setText(Coords.colIndexToLetter(col - 1).toString.toUpperCase))
  }

  for (row <- 0 to 7) {
    indexToLabel.get(row, 0).foreach(_._1.setText((8 - row).toString))
  }

  private def insertLabels(): Unit = {
    for (row <- 0 to 8) {
      for (col <- 0 to 8) {
        val label = new JLabel()
        label.setOpaque(true)
        label.setHorizontalAlignment(SwingConstants.CENTER)
        label.setVerticalAlignment(SwingConstants.CENTER)
        label.setFont(new Font("Sans", Font.PLAIN, 40))
        var color = lightColor
        if (row < 8 && col > 0) {
          if (row % 2 == 1) {
            if (col % 2 == 1) {
              color = darkColor
            }
          } else {
            if (col % 2 == 0) {
              color = darkColor
            }
          }

          label.setBackground(color)
          label.addMouseListener(new MouseAdapter {
            override def mousePressed(mouseEvent: MouseEvent): Unit = {
              fieldClicked(label, color, row, col)
            }
          })
        }
        add(label)
        indexToLabel += ((row, col) -> (label, color))
      }
    }
  }

  private def fieldClicked(label: JLabel, color: Color, row: Int, col: Int): Unit = {
    if (selectedField.contains((row, col))) {
      label.setBackground(color)
      selectedField = Option.empty
    } else if (selectedField.isEmpty) {
      val (pieceRow, pieceCol) = labelCoordToPieceCoord(row, col)
      val piece = board.getPiece(pieceRow, pieceCol)
      if (piece != null && piece.color == playerColor) {
        label.setBackground(selectedColor)
        selectedField = Option((row, col))
      }
    } else {
      val (toRow, toCol) = labelCoordToPieceCoord(row, col)
      val (fromRow, fromCol) = labelCoordToPieceCoord(selectedField.get._1, selectedField.get._2)
      val piece = board.getPiece(fromRow, fromCol)
      board = board.withMovedPiece(new Move(piece, fromRow, fromCol, toRow, toCol))
      board = board.withCastling(board.castlingState.afterMoveOf(piece, fromCol))
      indexToLabel.get(selectedField.get).foreach(labelColor => labelColor._1.setBackground(labelColor._2))
      selectedField = Option.empty
      setBoard(board)
    }

  }

  private def pieceCoordToLabelCoord(row: Int, col: Int): (Int, Int) = {
    val labelRow = 7 - row
    val labelCol = col + 1
    (labelRow, labelCol)
  }

  private def labelCoordToPieceCoord(row: Int, col: Int): (Int, Int) = (7 - row, col - 1)

  def setBoard(board: Board): Unit = {
    this.board = board
    for (row <- 0 to 7) {
      for (col <- 0 to 7) {
        val piece = board.getPiece(row, col)
        indexToLabel.get(pieceCoordToLabelCoord(row, col)).foreach(_._1.setText(
          if (piece != null) piece.toSymbol.toString else ""))
      }
    }
  }
}
