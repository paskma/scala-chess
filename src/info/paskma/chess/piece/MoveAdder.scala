package info.paskma.chess.piece

import info.paskma.chess.board.{Move, Color, Board}
import scala.collection.immutable.VectorBuilder

/**
 * User: paskma
 * Date: 5/31/14
 */
trait MoveAdder {
  /** returns true if we can add the move */
  protected def addMoveIfPossible(
                                   piece : Piece,
                                   fromRow : Int,
                                   fromCol : Int,
                                   toRow : Int,
                                   toCol : Int,
                                   board : Board,
                                   result : VectorBuilder[Move]) : Boolean = {

    if (board.isFree(toRow, toCol)) {
      result += new Move(piece, fromRow, fromCol, toRow, toCol)
      return true
    } else if (board.isPieceOfColor(toRow, toCol, Color.other(piece.color))) {
      result += new Move(piece, fromRow, fromCol, toRow, toCol)
      return false
    } else {
      return false
    }
  }

}
