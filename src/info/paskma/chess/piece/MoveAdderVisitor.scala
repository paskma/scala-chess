package info.paskma.chess.piece

import scala.collection.immutable.VectorBuilder
import info.paskma.chess.board.{Board, Move}

/**
 * User: paskma
 * Date: 6/10/14
 */
class MoveAdderVisitor(val result : VectorBuilder[Move]) extends Visitor with MoveAdder {

  override def visit(piece : Piece,
            fromRow : Int,
            fromCol : Int,
            toRow : Int,
            toCol : Int,
            board : Board) : Boolean = {
    addMoveIfPossible(piece, fromRow, fromCol, toRow, toCol, board, result)
  }
}
