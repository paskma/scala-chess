package info.paskma.chess.piece

import scala.util.control.Breaks._
import info.paskma.chess.board.{Move, Board}
import scala.collection.immutable.VectorBuilder

/**
 * User: paskma
 * Date: 6/1/14
 */
trait RookMovement {

  protected def enumerateRookMoves(piece : Piece, board : Board, row : Int, col : Int, visitor : Visitor) = {

    breakable { for (x <- row + 1 to 7) {
      if (!visitor.visit(piece, row, col, x, col, board))
        break
    }}

    breakable { for (x <- row - 1 to 0 by -1) {
      if (!visitor.visit(piece, row, col, x, col, board))
        break
    }}

    breakable { for (x <- col + 1 to 7) {
      if (!visitor.visit(piece, row, col, row, x, board))
        break
    }}

    breakable { for (x <- col - 1 to 0 by -1) {
      if (!visitor.visit(piece, row, col, row, x, board))
        break
    }}
  }
}
