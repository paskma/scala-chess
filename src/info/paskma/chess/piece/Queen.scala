package info.paskma.chess.piece

import info.paskma.chess.board.{Move, Color, Board}
import Color.Color

/**
 * User: paskma
 * Date: 6/1/14
 */
class Queen(color : Color) extends Piece(color, 10) with RookMovement with BishopMovement with RegularPiece {
  override def toString = "Queen(" + color + ")"
  override def toSymbol = if (isWhite) Queen.symbolWhite else Queen.symbolBlack

  protected def enumerate(board : Board, row : Int, col : Int, visitor : Visitor): Unit = {
    enumerateBishopMoves(this, board, row, col, visitor)
    enumerateRookMoves(this, board, row, col, visitor)
  }
}

object Queen {
  val symbolWhite = '♕'
  val symbolBlack = '♛'
}