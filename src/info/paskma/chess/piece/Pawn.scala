package info.paskma.chess.piece

import info.paskma.chess.board.{Move, Color, Board}
import Color._
import scala.collection.immutable.{VectorBuilder, Vector}

class Pawn(color : Color) extends Piece(color, 1) {

  override def toString = "Pawn(" + color + ")"
  override def toSymbol = if (isWhite) Pawn.symbolWhite else Pawn.symbolBlack
  
  def availableMoves(board : Board, i : Int, j : Int): Vector[Move] = {
    var result = new VectorBuilder[Move]
    
    assert(board.getPiece(i, j) == this)
    
    if (isWhite) {
      if (isValid(i + 1, j) && board.isFree(i + 1, j))
        result += new Move(this, i, j, i + 1, j)
      
      if (i == 1 && board.isFree(i + 1 to i + 2, j))
        result += new Move(this, i, j, i + 2, j)
      
      if (isValid(i + 1, j + 1) && board.isBlackPiece(i + 1, j + 1))
        result += new Move(this, i, j, i + 1, j + 1)
        
      if (isValid(i + 1, j -1) && board.isBlackPiece(i + 1, j - 1))
        result += new Move(this, i, j, i + 1, j-1)
        
      /* TODO: upgrade to stronger piece when we reach the last row */ 
    } else {
      if (isValid(i - 1, j) && board.isFree(i - 1, j))
        result += new Move(this, i, j, i - 1, j)
      
      if (i == 6 && board.isFree(i - 2 to i - 1, j))
        result += new Move(this, i, j, i - 2, j)
      
      if (isValid(i - 1, j + 1) && board.isWhitePiece(i - 1, j + 1))
        result += new Move(this, i, j, i - 1, j + 1)
        
      if (isValid(i - 1, j -1) && board.isWhitePiece(i - 1, j - 1))
        result += new Move(this, i, j, i - 1, j-1)
      
      /* TODO: upgrade to stronger piece when we reach the first row */
    }
    
    return result.result()
  }

  def coverage(board : Board, row : Int, col : Int) : Int = 0
}

object Pawn {
  val symbolWhite = '♙'
  val symbolBlack = '♟'
}