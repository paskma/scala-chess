package info.paskma.chess.piece

import info.paskma.chess.board.{Color, Move, Board}
import scala.collection.immutable.VectorBuilder

/**
 * User: paskma
 * Date: 6/11/14
 */
class CoverageVisitor extends Visitor {
  var result : Int = 0

  override def visit(piece : Piece,
                     fromRow : Int,
                     fromCol : Int,
                     toRow : Int,
                     toCol : Int,
                     board : Board) : Boolean = {
    if (board.isFree(toRow, toCol)) {
      result += 1
      return true
    } else if (board.isPieceOfColor(toRow, toCol, Color.other(piece.color))) {
      result += 1
      return true
    } else {
      return false
    }
  }

}
