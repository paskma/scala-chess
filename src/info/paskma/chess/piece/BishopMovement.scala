package info.paskma.chess.piece

import scala.util.control.Breaks._
import info.paskma.chess.board.{Move, Board}
import scala.collection.immutable.VectorBuilder

/**
 * User: paskma
 * Date: 6/1/14
 */
trait BishopMovement {
  protected def enumerateBishopMoves(piece : Piece, board : Board, row : Int, col : Int, visitor : Visitor) = {
    breakable { for (x <- 1 to math.min(7 - row, 7 - col)) {
      if (!visitor.visit(piece, row, col, row + x, col + x, board))
        break
    }}

    breakable { for (x <- 1 to math.min(7 - row, col)) {
      if (!visitor.visit(piece, row, col, row + x, col - x, board))
        break
    }}

    breakable { for (x <- 1 to math.min(row, 7 - col)) {
      if (!visitor.visit(piece, row, col, row - x, col + x, board))
        break
    }}

    breakable { for (x <- 1 to math.min(row, col)) {
      if (!visitor.visit(piece, row, col, row - x, col - x, board))
        break
    }}
  }
}