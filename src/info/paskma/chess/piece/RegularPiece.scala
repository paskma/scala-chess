package info.paskma.chess.piece

import info.paskma.chess.board.{Move, Board}
import scala.collection.immutable.VectorBuilder

/**
 * User: paskma
 * Date: 6/11/14
 */
trait RegularPiece {
  def availableMoves(board : Board, row : Int, col : Int): Vector[Move] = {
    val visitor = new MoveAdderVisitor(new VectorBuilder[Move]())
    enumerate(board, row, col, visitor)
    visitor.result.result()
  }

  protected def enumerate(board : Board, row : Int, col : Int, visitor : Visitor): Unit

  def coverage(board : Board, row : Int, col : Int) : Int = {
    val visitor = new CoverageVisitor
    enumerate(board, row, col, visitor)
    visitor.result
  }
}
