package info.paskma.chess.piece

import info.paskma.chess.board._
import Color._
import scala.collection.immutable.{VectorBuilder, Vector}
import info.paskma.chess.board.Coords._

/**
 * In this model, king is just ordinary piece with extraordinary value.
 */
class King(color : Color) extends Piece(color, 1000000000) with MoveAdder {

  override def toString = "King(" + color + ")"
  override def toSymbol = if (isWhite) King.symbolWhite else King.symbolBlack

  override def availableStraitMoves(board : Board, row : Int, col : Int): Vector[Move] = {
    val result = new VectorBuilder[Move]()
    
    for (i <- math.max(row - 1, 0) to math.min(row + 1, 7)) {
      for (j <- math.max(col - 1, 0) to math.min(col + 1, 7)) {
        addMoveIfPossible(this, row, col, i, j, board, result)
      }
    }
    
    return result.result()
  }

  def availableMoves(board : Board, row : Int, col : Int): Vector[Move] = {
    return availableStraitMoves(board, row, col) ++ castlingMoves(board, row, col)
  }

  def isRook(board : Board, row : Int, col : Int, color : Color) : Boolean = {
    val p = board.getPiece(row, col)
    if (p == null || !p.isInstanceOf[Rook])
      return false

    return p.color == color
  }

  private def castlingMoves(board : Board, row : Int, col : Int): Vector[Move] = {
    val result = new VectorBuilder[Move]()
    // this is not entirely correct for pawns
    lazy val booleanCoverage : Array[Array[Boolean]] = board.booleanCoverage(Color.other(color))

    if (isWhite && board.castlingState.whiteKingSideAvailable && board.isFree(ROW_1, COL_F to COL_G)
      && isRook(board, ROW_1, COL_H, White) && !anyCovered(booleanCoverage, ROW_1, COL_E to COL_G)) {
      assert(row == ROW_1)
      result += new CastlingMove(this, row, col, row, col + 2)
    }

    if (isWhite && board.castlingState.whiteQueenSideAvailable && board.isFree(ROW_1, COL_B to COL_D)
      && isRook(board, ROW_1, COL_A, White) && !anyCovered(booleanCoverage, ROW_1, COL_C to COL_E)) {
      assert(row == ROW_1)
      result += new CastlingMove(this, row, col, row, col - 2)
    }

    if (isBlack && board.castlingState.blackKingSideAvailable && board.isFree(ROW_8, COL_F to COL_G)
      && isRook(board, ROW_8, COL_H, Black) && !anyCovered(booleanCoverage, ROW_8, COL_E to COL_G)) {
      assert(row == ROW_8)
      result += new CastlingMove(this, row, col, row, col + 2)
    }

    if (isBlack && board.castlingState.blackQueenSideAvailable && board.isFree(ROW_8, COL_B to COL_D)
      && isRook(board, ROW_8, COL_A, Black) && !anyCovered(booleanCoverage, ROW_8, COL_C to COL_E)) {
      assert(row == ROW_8)
      result += new CastlingMove(this, row, col, row, col - 2)
    }

    return result.result()
  }


  protected def anyCovered(coverage : Array[Array[Boolean]], row : Int, cols : Range) : Boolean = {
    for (col <- cols) {
      if (coverage(row)(col))
        return true
    }

    false
  }

  def coverage(board : Board, row : Int, col : Int) : Int = 0
}

object King {
  val symbolWhite = '♔'
  val symbolBlack = '♚'
}