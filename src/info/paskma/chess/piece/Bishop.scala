package info.paskma.chess.piece

import info.paskma.chess.board.{Move, Color, Board}
import Color._

/**
 * User: paskma
 * Date: 5/31/14
 */
class Bishop(color : Color) extends Piece(color, 3) with BishopMovement with RegularPiece {

  override def toString = "Bishop(" + color + ")"
  override def toSymbol = if (isWhite) Bishop.symbolWhite else Bishop.symbolBlack

  protected def enumerate(board : Board, row : Int, col : Int, visitor : Visitor): Unit = {
    enumerateBishopMoves(this, board, row, col, visitor)
  }
}


object Bishop {
  val symbolWhite = '♗'
  val symbolBlack = '♝'
}