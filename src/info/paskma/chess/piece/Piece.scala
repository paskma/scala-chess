package info.paskma.chess.piece

import info.paskma.chess.board.{Validation, Move, Color, Board}
import Color._

abstract class Piece(val color : Color, val weight : Long) extends Validation {

  def toSymbol : Char

  def weightScore : Long = {
    color match {
      case White => weight
      case Black => -weight
    }
  }
  
  def isWhite = color == White
  def isBlack = color == Black


  def availableMoves(board : Board, i : Int, j : Int): Vector[Move]

  def availableStraitMoves(board : Board, i : Int, j : Int): Vector[Move] = availableMoves(board, i, j)

  def coverage(board : Board, row : Int, col : Int) : Int

  def coverageScore(board : Board, row : Int, col : Int) : Int = {
    color match {
      case White => coverage(board, row, col)
      case Black => -coverage(board, row, col)
    }
  }
}