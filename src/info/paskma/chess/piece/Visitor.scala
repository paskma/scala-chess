package info.paskma.chess.piece

import info.paskma.chess.board.Board

/**
 * User: paskma
 * Date: 6/10/14
 */
trait Visitor {
  def visit(piece : Piece,
            fromRow : Int,
            fromCol : Int,
            toRow : Int,
            toCol : Int,
            board : Board) : Boolean
}
