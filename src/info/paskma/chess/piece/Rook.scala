package info.paskma.chess.piece

import info.paskma.chess.board.{Color, Board}
import Color._

class Rook(color : Color) extends Piece(color, 5) with RookMovement with RegularPiece {
  
  override def toString = "Rook(" + color + ")"
  override def toSymbol = if (isWhite) Rook.symbolWhite else Rook.symbolBlack

  protected def enumerate(board : Board, row : Int, col : Int, visitor : Visitor): Unit = {
    enumerateRookMoves(this, board, row, col, visitor)
  }
}


object Rook {
  val symbolWhite = '♖'
  val symbolBlack = '♜'
}