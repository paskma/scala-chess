package info.paskma.chess.piece

import info.paskma.chess.board.{Move, Color, Board}
import Color.Color

/**
 * User: paskma
 * Date: 6/1/14
 */
class Knight(color : Color) extends Piece(color, 3) with RegularPiece {

  override def toString = "Knight(" + color + ")"
  override def toSymbol = if (isWhite) Knight.symbolWhite else Knight.symbolBlack

  protected def enumerate(board : Board, row : Int, col : Int, visitor : Visitor): Unit = {
    assert(board.getPiece(row, col) == this)
    val rawMoves = List(
      (row + 2, col + 1),
      (row + 2, col - 1),
      (row - 2, col + 1),
      (row - 2, col - 1),
      (row + 1, col + 2),
      (row + 1, col - 2),
      (row - 1, col + 2),
      (row - 1, col - 2)
    )

    def exerciseMove(toRow : Int, toCol : Int) = {
      if (board.isValid(toRow, toCol)) {
        visitor.visit(this, row, col, toRow, toCol, board)
      }
    }

    rawMoves.foreach(x => exerciseMove(x._1, x._2))
  }
}

object Knight {
  val symbolWhite = '♘'
  val symbolBlack = '♞'
}