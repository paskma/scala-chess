package info.paskma.chess

import info.paskma.chess.io.BoardIo
import info.paskma.chess.board.{Move, Coords}

/**
 * User: paskma
 * Date: 6/19/14
 */
object ApplyMove {
  val usage = "ApplyMove <boardfile> <from> <to>"

  def main(args: Array[String]) : Unit = {
    if (args.length == 0) {
      println(usage)
      return
    }

    val board = BoardIo.fromFile(args(0))
    val moveFrom = Coords.parseCoord(args(1))
    val moveTo = Coords.parseCoord(args(2))

    val piece = board.getPiece(moveFrom._1, moveFrom._2)
    val movedBoard = board.withMovedPiece(new Move(piece, moveFrom._1, moveFrom._2, moveTo._1, moveTo._2))

    println(movedBoard)
  }
}
